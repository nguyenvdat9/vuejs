/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
 
require('./bootstrap');


import VueRouter from 'vue-router';

import Vue from 'vue';
window.Vue = require('vue');
Vue.use(VueRouter);


 import VueAxios from 'vue-axios';
 import axios from 'axios';

 import App from './App.vue';
 Vue.use(VueAxios,axios);


 import routes from './routes.js';
 import { initialize } from "./helpers/general";

// Vue.component('home',require('./components/HomeComp'));
const router = new VueRouter({mode: 'history',routes: routes});
import store from './store';
initialize(store, router);


// app.use(cors(corsOptions));
const app = new Vue({
    el: '#app',
    render : h => h(App),
    router,
    store: store,
  
});

// const app = new Vue(Vue.util.extend({ router }, App,store)).$mount('#app');
