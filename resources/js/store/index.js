import Vue from 'vue';
import Vuex from 'vuex';

import users from './modules/users';
import auth from './modules/auth';

Vue.use(Vuex);

const storeData = {
    modules: {
        users,
        auth,
    },
}

const store = new Vuex.Store(storeData);

export default store;