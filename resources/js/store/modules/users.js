import axios from "axios";

const UserModules = {
    state: {
        users : [],
        user: {},
        admin: {},
    },
    actions: {
        async addUsers({commit}, newUser) {
       
                await axios.post(`http://127.0.0.1:8000/api/user/create`,newUser)
                .then((response) => {
                    alert(response.data.success); 
                    commit('ADD_USER',newUser);              
                }
               );
                
        
        },
        async editUser({commit},userId){
            const response = await axios.get(`http://127.0.0.1:8000/api/user/edit/${userId}`)
            commit('EDIT_USER',response.data.user);
        } ,
        async updateUser({commit},user){
            try{
                await axios.post(`http://127.0.0.1:8000/api/user/update/${user.id}`,user)
                .then(response =>{
                    alert(response.data.success);
                })
                
            }catch(e){
                console.log(e)
            }
            
            
        },
        async getUser({commit}){
            try {
                const response = await axios.post(`http://127.0.0.1:8000/api/users`);
                commit('SET_USERS', response.data.data);
            }catch(e){
                console.log(e)
            }
        }
    }, 
    getters: {
        users : state => state.users,
        user: state => state.user
    }, 
    mutations: {
        ADD_USER(state, newUser) {
            state.users.unshift(newUser);
        },
        SET_USERS(state,users){
            state.users = users
        },
        EDIT_USER(state, user) {
            state.user = user;
        }
    }
}
export default UserModules;
