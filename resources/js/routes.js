import HomeComp from  './components/HomeComp';
import IndexComp from  './components/IndexComp';
import CreateUserComp from  './components/CreateUserComp';
import EditUserComp from  './components/EditUserComp';
import Login from  './components/auth/login';

const routes = [
    { 
        path: '/',
        component: HomeComp, 
        name: 'home', 
    },{
        name: 'index',
        component: IndexComp, 
        path: '/user',
    },
    {
        name: 'create',
        component: CreateUserComp, 
        path: '/create',
    },
    {
        name: 'edit',
        component: EditUserComp,  
        path: '/edit/:id',
    },
    {
        path: '/login',
        component: Login,
        name: 'login'
    }
]

export default routes;