<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository {
    protected $user;

    public function __construct(User $user) {
        $this->user = $user;
    }
    public function insert(array $attributes) {
        return $this->user->insert($attributes);
    }
    public function update($id,array $user_data) {
        return User::find($id)->update($user_data);
    }
    public function index($search) {
        if($search){
            return $this->user->orderBy('name', 'ASC')->where('name', 'like', '%'.$search.'%')->get();
        }else{
           return $this->user->orderBy('name', 'ASC')->get(); 
        }
        
    }
    public function delete($id) {
        return User::find($id)->delete();
    }
}