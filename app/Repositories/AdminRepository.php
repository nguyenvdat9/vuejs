<?php
 
 namespace App\Repositories;

use App\Models\Admin;

class AdminRepository {
     protected $admin;

     public function __construct(Admin $admin) {
        $this->admin = $admin;
     }
     public function createNewToken($token) {
      return response()->json([
         'access_token' => $token,
         'token_type' => 'bearer',
         'expires_in' => auth()->factory()->getTTL() * 60,
         'user' => auth()->user()
     ]);
      }  
 }