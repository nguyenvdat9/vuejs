<?php
 namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserService {
     public function __construct(UserRepository $userRepository) {
         $this->userRepository = $userRepository;
     }
     public function create(Request $request) {
        $password =Hash::make($request->password);
        $request->merge(['password' => $password]);
        $data = $request->all();
        if($this->userRepository->insert($data)){
            return response()->json(['success' =>'Success']);
        }
     }
     public function update(Request $request, $id){
         $user_data = $request->only('name','address','phone','email','password');
         $this->userRepository->update($id,$user_data);
     }
     public function index($search) {
         return $this->userRepository->index($search);
     }
     public function delete($id){
         $this->userRepository->delete($id);
     }
 }