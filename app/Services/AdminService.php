<?php

namespace App\Services;

use App\Repositories\AdminRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdminService {
    public function __construct(AdminRepository $adminRepository){
        $this->adminRepository = $adminRepository;
    }
    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);
        
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (!$token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
       
        return $this->adminRepository->createNewToken($token);
    }
    public function createNewToken($token) {
        return $this->adminRepository->createNewToken($token);
    }
}