<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

        Route::post('/user/create', 'Api\UserController@store');
Route::get('/user/edit/{id}', 'Api\UserController@edit');
Route::post('/user/update/{id}', 'Api\UserController@update');
Route::delete('/user/delete/{id}', 'Api\UserController@destroy');
Route::post('/users', 'Api\UserController@index');
Route::post('/search', 'Api\UserController@index');
   


Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('/login', 'Api\AuthController@login')->name('login');
    Route::post('/logout', 'Api\AuthController@logout')->name('logout');
    Route::post('/register', 'Api\AuthController@register')->name('register');
    Route::get('/user', 'Api\AuthController@user')->name('user');
});
